﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace cetvrti
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
