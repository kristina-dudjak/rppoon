﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace cetvrti
{
    class LoggingProxyDataset:IDataset
    {
        private string filePath;
        private Dataset dataset;

        public LoggingProxyDataset(string filePath)
        {
            this.filePath = filePath;
        }

        public ReadOnlyCollection<List<string>> GetData()
        {
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            ConsoleLogger.GetInstance().Log(" Logged in: "+DateTime.Now);
            return dataset.GetData();
        }
    }
}
