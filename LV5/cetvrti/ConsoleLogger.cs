﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cetvrti
{
    class ConsoleLogger
    {
        static private ConsoleLogger instance;

        private ConsoleLogger(){ }

        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
            {
                instance = new ConsoleLogger();
            }
            return instance;
        }
        
        public void Log(string message)
        {
            Console.WriteLine(message);
        }

       
    }
    
}
