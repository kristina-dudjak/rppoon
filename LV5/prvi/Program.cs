﻿using System;

namespace prvi
{
    class Program
    {
        static void Main(string[] args)
        {
            Product sweater = new Product("white sweater",150,1);
            Product blazer = new Product("black blazer",300,1);
            Product boots = new Product("brown boots", 400, 4);

            Box box = new Box("your order:");

            box.Add(sweater);
            box.Add(blazer);
            box.Add(boots);

            Console.WriteLine(box.Description());
        }
    }
}
