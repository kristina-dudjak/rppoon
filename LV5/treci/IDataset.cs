﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace treci
{
    interface IDataset
    {
        ReadOnlyCollection<List<string>> GetData();
    }
}
