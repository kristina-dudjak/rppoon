﻿using System;
using System.Collections.Generic;
using System.Text;

namespace treci
{
    class DataConsolePrinter
    {
        public void Print(IDataset dataset)
        {
            if (dataset.GetData() != null)
            {
                foreach (List<string> list in dataset.GetData())
                {
                    Console.WriteLine(string.Join(",", list));
                }
                Console.WriteLine();
            }
        }
    }
}


