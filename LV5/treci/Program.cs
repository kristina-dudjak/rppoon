﻿using System;

namespace treci
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset(
                "C:\\Users\\Antonija\\source\\repos\\RPPOON\\LV5\\treci\\CSVFile.txt");


            VirtualProxyDataset virtualproxy = new VirtualProxyDataset(
                "C:\\Users\\Antonija\\source\\repos\\RPPOON\\LV5\\treci\\CSVFile.txt");


            ProtectionProxyDataset protectionproxy = new ProtectionProxyDataset(User.GenerateUser("Kristina"));
           

            DataConsolePrinter dataConsolePrinter = new DataConsolePrinter();

            dataConsolePrinter.Print(dataset);
            dataConsolePrinter.Print(virtualproxy);
            dataConsolePrinter.Print(protectionproxy);
        }
    }
}
