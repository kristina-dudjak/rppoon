﻿using System;

namespace peti
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lightTheme = new LightTheme();
            DarkTheme darkTheme = new DarkTheme();

            ReminderNote firstNote = new ReminderNote("Homework-English 1 due to Wednesday!", lightTheme);
            ReminderNote secondNote = new ReminderNote("KM quiz due to Sunday!", darkTheme);

            firstNote.Show();
            secondNote.Show();
        }
    }
}
