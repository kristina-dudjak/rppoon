﻿using System;

namespace sesti
{
    class Program
    {
        static void Main(string[] args)
        {
            LightTheme lightTheme = new LightTheme();
            DarkTheme darkTheme = new DarkTheme();
            ReminderNote friendreminder = new ReminderNote("Coffee at 6 o'clock!", lightTheme);
            friendreminder.Show();

            GroupNote Friends = new GroupNote("Monica Phoebe Rachel Joey Ross Chandler Gunther", lightTheme);
            Friends.Remove("Gunther");
            Friends.Show();

            ReminderNote reminder = new ReminderNote("Comic con on Friday!", darkTheme);
            reminder.Show();
            GroupNote BigBang = new GroupNote("Sheldon Raj Leonard Howard ",darkTheme);
            BigBang.Show();
        }
    }
}
