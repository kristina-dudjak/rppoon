﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sedmi
{
    class GroupNote:Note
    {
        private List<string> notes = new List<string>();

        public GroupNote(string message, ITheme theme) : base(message, theme)
        {
            for (int i = 0; i < message.Split(" ").Length; i++)
            {
                notes.Add(message.Split(" ")[i]);
            }
        }

        public void Add(string note)
        {
            notes.Add(note);
        }

        public void Remove(string note)
        {
            notes.Remove(note);
        }

        public override void Show()
        {
            this.ChangeColor();
            foreach (string note in notes)
            {
                Console.WriteLine(note.ToString());
            }
            Console.ResetColor();
        }
    }
}
