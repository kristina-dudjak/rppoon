﻿using System;

namespace sedmi
{
    class Program
    {
        static void Main(string[] args)
        {
            DarkTheme darkTheme = new DarkTheme();
            LightTheme lightTheme = new LightTheme();

            ReminderNote friendreminder = new ReminderNote("Coffee at 6 o'clock!", lightTheme);
            ReminderNote reminder = new ReminderNote("Comic con on Friday!", darkTheme);

            GroupNote BigBang = new GroupNote("Sheldon Raj Leonard Howard", darkTheme);
            GroupNote Friends= new GroupNote("Phoebe Rachel Monica Ross Chandler Joey", lightTheme);

            Notebook notebook = new Notebook(darkTheme);
            notebook.AddNote(reminder);
            notebook.AddNote(BigBang);
            notebook.AddNote(friendreminder);
            notebook.AddNote(Friends);
            notebook.Display();

            notebook.ChangeTheme(lightTheme);
            notebook.Display();
        }
    }
}
