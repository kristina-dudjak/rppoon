﻿using System;

namespace drugi
{
    class Program
    {
        static void Main(string[] args)
        {
            Product Book = new Product("Cookbook", 100, 4);
            Product Blender = new Product("Blender", 300, 6);

            Box box = new Box("your order:");

            box.Add(Book);
            box.Add(Blender);

            ShippingService shippingService = new ShippingService(5);

            Console.WriteLine(box.Description());
            Console.WriteLine("Shipping cost: "+shippingService.GetShippingPrice(box));
        }
    }
}
