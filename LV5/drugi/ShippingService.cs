﻿using System;
using System.Collections.Generic;
using System.Text;

namespace drugi
{
    class ShippingService
    {
        private double PricePerKg;

        public double PricePerkg
        {
            get => PricePerKg;
            set => PricePerKg = value;
        }

        public ShippingService(double pricePerKg)
        {
            this.PricePerKg = pricePerKg;
        }

        public double GetShippingPrice(IShipable item)
        {
            return item.Weight * PricePerKg;
        }
    }
}
