﻿using System;
using System.Collections.Generic;
using System.Text;

namespace treci
{
    class CareTaker
    {
        private Stack<Memento> states = new Stack<Memento>();
        
        public void AddMemento(Memento memento)
        {
            states.Push(memento);
        }
        public void RemoveMemento()
        {
            states.Pop();
        }
        public Memento GetMemento()
        {
            return states.Pop();
        }
    }
}
