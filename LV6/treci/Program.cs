﻿using System;

namespace treci
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem toDoItem = new ToDoItem("KM ", "quiz", new DateTime(2020, 5, 14, 18, 00, 00));

            careTaker.AddMemento(toDoItem.StoreState());
            Console.WriteLine(toDoItem.ToString());

            toDoItem.Rename("VIS");
            careTaker.AddMemento(toDoItem.StoreState());
            Console.WriteLine(toDoItem.ToString());

            toDoItem.ChangeTask("class");
            careTaker.AddMemento(toDoItem.StoreState());
            Console.WriteLine(toDoItem.ToString());

            toDoItem.RestoreState(careTaker.GetMemento());
            Console.WriteLine(toDoItem.ToString());

            toDoItem.RestoreState(careTaker.GetMemento());
            Console.WriteLine(toDoItem.ToString());
        }
    }
}
