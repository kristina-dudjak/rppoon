﻿using System;

namespace sedmi
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker checker = new StringLengthChecker(8);

            PasswordValidator passwordValidator = new PasswordValidator(checker);

            passwordValidator.AddStringChecker(new StringDigitChecker());
            passwordValidator.AddStringChecker(new StringLowerCaseChecker());
            passwordValidator.AddStringChecker(new StringUpperCaseChecker());

            string valid = "Kristina123";
            string invalid = "kristina123";

            Console.WriteLine(valid);
            Console.WriteLine("passable: "+passwordValidator.Validate(valid));
            
            Console.WriteLine(invalid);
            Console.WriteLine("passable: " + passwordValidator.Validate(invalid));

        }
    }
}
