﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sedmi
{
    class PasswordValidator
    {
        private StringChecker first;
        private StringChecker last;

        public PasswordValidator(StringChecker first)
        {
            this.first = first;
            this.last = first;
        }

        public void AddStringChecker(StringChecker stringChecker)
        {
            last.SetNext(stringChecker);
            last = stringChecker;
        }

        public bool Validate(string password)
        {
            return first.Check(password);
        }

    }
}
