﻿using System;

namespace peti
{
    class Program
    {
        static void Main(string[] args)
        {
            AbstractLogger logger = new ConsoleLogger(MessageType.ALL);
            FileLogger fileLogger =
                new FileLogger(MessageType.ERROR | MessageType.WARNING, "logFile.txt");

            logger.SetNextLogger(fileLogger);

            logger.Log("poruka", MessageType.INFO);  //ta se nece ispisati u file
            logger.Log("poruka", MessageType.WARNING);
            logger.Log("poruka", MessageType.ERROR);


        }
    }
}
