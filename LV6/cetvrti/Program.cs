﻿using System;

namespace cetvrti
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount bankAccount = new BankAccount("Kristina", "Kristina ulica", 2000);

            Memento memento=bankAccount.StoreState();
            Console.WriteLine(bankAccount.ToString());

            bankAccount.UpdateBalance(200);
            Console.WriteLine(bankAccount.ToString());

            bankAccount.RestoreState(memento);
            Console.WriteLine(bankAccount.ToString());



        }
    }
}
