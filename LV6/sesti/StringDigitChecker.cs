﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sesti
{
    class StringDigitChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            char[] array=stringToCheck.ToCharArray();
            foreach(char c in array)
            {
                if (Char.IsDigit(c) == true)
                {
                    return true;
                }
            }
            return false;
        }

    }
}
