﻿using System;

namespace sesti
{
    class Program
    {
        static void Main(string[] args)
        {
            StringChecker checker = new StringLengthChecker(8);
            StringDigitChecker stringDigitChecker = new StringDigitChecker();
            StringLowerCaseChecker stringLowerCaseChecker = new StringLowerCaseChecker();
            StringUpperCaseChecker stringUpperCaseChecker = new StringUpperCaseChecker();

            checker.SetNext(stringDigitChecker);
            stringDigitChecker.SetNext(stringLowerCaseChecker);
            stringLowerCaseChecker.SetNext(stringUpperCaseChecker);

            string valid = "Kristina123";
            string invalid = "kristina123";

            Console.WriteLine(valid);
            Console.WriteLine("passable: "+checker.Check(valid));

            Console.WriteLine(invalid);
            Console.WriteLine("passable: "+checker.Check(invalid));
        }
    }
}
