﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sesti
{
    class StringLowerCaseChecker:StringChecker
    {
        protected override bool PerformCheck(string stringToCheck)
        {
            char[] array = stringToCheck.ToCharArray();
            foreach(char c in array)
            {
                if (Char.IsLower(c) == true)
                {
                    return true;
                }
            }
            return false;
        }
    }
}