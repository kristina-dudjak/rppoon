﻿using System;

namespace prvi
{
    class Program
    {
        static void Main(string[] args)
        {
            Note firstnote = new Note("Water sports", "canoeing kayaking water polo");
            Note secondnote = new Note("Ice sports", "skiing sledding ice skating");
            Note thirdnote = new Note("Indoor sports", "bowling volleyball badminton");
            Notebook notebook = new Notebook();
            
            notebook.AddNote(firstnote);
            notebook.AddNote(secondnote);
            notebook.AddNote(thirdnote);

            IAbstractIterator iterator= notebook.GetIterator();

            for (Note note = iterator.First(); note != null; note = iterator.Next())
            {
                note.Show();
            }

            notebook.RemoveNote(thirdnote);
            iterator = notebook.GetIterator();
            for (Note note = iterator.First(); note != null; note = iterator.Next())
            {
                note.Show();
            }
            
        }
    }
}
