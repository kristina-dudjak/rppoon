﻿using System;

namespace drugi
{
    class Program
    {
        static void Main(string[] args)
        {
            Product cutlery = new Product("6-piece cutlery kit", 100);
            Product dinnerware = new Product("12-piece dinnerware set", 200);
            Product glassware = new Product("water jug and 6 glasses", 100);

            Box box = new Box();
            box.AddProduct(cutlery);
            box.AddProduct(dinnerware);
            box.AddProduct(glassware);

            IAbstractIterator iterator = box.GetIterator();

            for (Product product = iterator.First(); product != null; product = iterator.Next())
            {
                Console.WriteLine(product.ToString());
            }
            

        }
    }
}
