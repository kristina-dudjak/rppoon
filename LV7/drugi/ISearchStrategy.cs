﻿using System;
using System.Collections.Generic;
using System.Text;

namespace drugi
{
    interface ISearchStrategy
    {
        int Search(double[] array, double item);
    }
}
