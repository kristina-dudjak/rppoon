﻿using System;

namespace drugi
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[] { 1, 22, 3, 44, 5, 66, 7, 88, 9, 100, 11 };

            NumberSequence numberSequence = new NumberSequence(array);
            Console.WriteLine(numberSequence.ToString());

            double item = array[4];
            double number = 6;

            numberSequence.SetSearchStrategy(new LinearSearch());
            Console.WriteLine(numberSequence.Search(item));

            numberSequence.SetSearchStrategy(new LinearSearch());
            Console.WriteLine(numberSequence.Search(number));
        }
    }
}
