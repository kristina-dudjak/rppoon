﻿using System;
using System.Collections.Generic;
using System.Text;

namespace drugi
{
    class LinearSearch:ISearchStrategy
    {
        public int Search(double[] array, double item)
        {
            int arraySize = array.Length;
            for(int i = 0; i < arraySize; i++)
            {
                if (array[i] == item)
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
