﻿using System;
using System.Collections.Generic;
using System.Text;

namespace treci
{
    class ConsoleLogger:ILogger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            Console.WriteLine(DateTime.Now + " -> CPU load: " + 
                provider.CPULoad + " Available RAM: " + provider.AvailableRAM);
        }
    }
}
