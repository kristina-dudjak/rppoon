﻿using System;
using System.Threading;
namespace treci
{
    class Program
    {
        static void Main(string[] args)
        {
            SystemDataProvider systemDataProvider = new SystemDataProvider();

            ConsoleLogger consoleLogger = new ConsoleLogger();
            FileLogger fileLogger = new FileLogger("File.txt");

            systemDataProvider.Attach(consoleLogger);
            systemDataProvider.Attach(fileLogger);
            for (; ; )
            {
                systemDataProvider.GetAvailableRAM();
                systemDataProvider.GetCPULoad();
                Thread.Sleep(1000);
            }
        }
    }
}
