﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cetvrti
{
    class SystemDataProvider : SimpleSystemDataProvider
        {
            private float previousCPULoad;
            private float previousRAMAvailable;

            public SystemDataProvider() : base()
            {
                this.previousCPULoad = this.CPULoad;
                this.previousRAMAvailable = this.AvailableRAM;
        }
        
            public float GetCPULoad()
            {
                float currentLoad = this.CPULoad;
                float percentage = (Math.Abs(currentLoad - previousCPULoad) / previousCPULoad)*100;
                if (percentage>=10)
                {
                    this.Notify();
                }
                this.previousCPULoad = currentLoad;
                return currentLoad;
            }

            public float GetAvailableRAM()
            {
                float currentRAM = this.AvailableRAM;
                float percentage = (Math.Abs(currentRAM - previousRAMAvailable) / previousRAMAvailable) * 100;
                if (percentage>=10)
                {
                    this.Notify();
                }
                this.previousRAMAvailable = currentRAM;
                return currentRAM;
            }
        }
}
