﻿using System;

namespace prvi
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] array = new double[] { 1, 22,3,44,5,66,7,88,9,100,11};

            NumberSequence numberSequence = new NumberSequence(array);
            Console.WriteLine(numberSequence.ToString());

            numberSequence.SetSortStrategy(new SequentialSort());
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());

            numberSequence.SetSortStrategy(new CombSort());
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());

            numberSequence.SetSortStrategy(new BubbleSort());
            numberSequence.Sort();
            Console.WriteLine(numberSequence.ToString());
        }
    }
}
