﻿using System;

namespace peti
{
    class Program
    {
        static void Main(string[] args)
        {
            BuyVisitor buyVisitor = new BuyVisitor();
            Book book = new Book("Harry Potter", 15);
            DVD dvd = new DVD("Hacksaw Ridge", DVDType.MOVIE, 25);
            VHS vhs = new VHS("Rocky", 20);

            Console.WriteLine(book.ToString());
            Console.WriteLine("Price with tax: "+book.Accept(buyVisitor));

            Console.WriteLine(dvd.ToString());
            Console.WriteLine("Price with tax: " +dvd.Accept(buyVisitor));

            Console.WriteLine(vhs.ToString());
            Console.WriteLine("Price with tax: " +vhs.Accept(buyVisitor));
            
        }
    }
}
