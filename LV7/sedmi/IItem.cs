﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sedmi
{
    interface IItem
    {
        double Accept(IVisitor visitor);
    }
}
