﻿using System;

namespace sedmi
{
    class Program
    {
        static void Main(string[] args)
        {
            RentVisitor rentVisitor = new RentVisitor();
            BuyVisitor buyVisitor = new BuyVisitor();
            Book book = new Book("Harry Potter", 15);
            DVD dvd = new DVD("Hacksaw Ridge", DVDType.MOVIE, 25);
            DVD software = new DVD("Photoshop", DVDType.SOFTWARE, 30);
            VHS vhs = new VHS("Rocky", 20);
            
            Cart cart = new Cart();
            cart.Add(book);
            cart.Add(dvd);
            cart.Add(software);
            cart.Add(vhs);

            Console.WriteLine("Renting price with tax: " + cart.Accept(rentVisitor));
            Console.WriteLine("Buying price with tax: " + cart.Accept(buyVisitor));
        }
    }
}