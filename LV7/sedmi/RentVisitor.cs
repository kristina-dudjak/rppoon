﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sedmi
{
    class RentVisitor : IVisitor
    {
        private const double DVDTax = 0.18;
        private const double VHSTax = 0.10;
        private const double BookTax = 0.8;

        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type==DVDType.SOFTWARE)
            {
                return DVDItem.Price * (1 + DVDTax);
            }
            return 0.1* DVDItem.Price * (1 + DVDTax);
        }
        public double Visit(VHS VHSItem)
        {
            return 0.1 * VHSItem.Price * (1 + VHSTax);
        }
        public double Visit(Book BookItem)
        {
            return 0.1 * BookItem.Price * (1 + BookTax);
        }
    }
}
