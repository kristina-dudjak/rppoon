﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sedmi
{
    class Cart
    {
        private List<IItem> items;

        public Cart()
        {
            this.items = new List<IItem>();
        }

        public void Add(IItem item)
        {
            items.Add(item);
        }

        public void Remove(IItem item)
        {
            items.Remove(item);
        }

        public double Accept(IVisitor visitor)
        {
            double total=0;
            foreach(IItem item in items)
            {
                total += item.Accept(visitor);
            }
            return total;
        }
    }
}