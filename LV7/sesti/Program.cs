﻿using System;

namespace sesti
{
    class Program
    {
        static void Main(string[] args)
        {
            RentVisitor rentVisitor = new RentVisitor();
            Book book = new Book("Harry Potter", 15);
            DVD dvd = new DVD("Hacksaw Ridge", DVDType.MOVIE, 25);
            DVD software = new DVD("Photoshop", DVDType.SOFTWARE, 30);
            VHS vhs = new VHS("Rocky", 20);

            Console.WriteLine(book.ToString());
            Console.WriteLine("Price with tax: " + book.Accept(rentVisitor));

            Console.WriteLine(dvd.ToString());
            Console.WriteLine("Price with tax: " + dvd.Accept(rentVisitor));
            
            Console.WriteLine(software.ToString());
            Console.WriteLine("Price with tax: " + software.Accept(rentVisitor));

            Console.WriteLine(vhs.ToString());
            Console.WriteLine("Price with tax: " + vhs.Accept(rentVisitor));

        }
    }
}
