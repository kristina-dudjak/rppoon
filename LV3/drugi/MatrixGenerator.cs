﻿using System;
using System.Collections.Generic;
using System.Text;

namespace drugi
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private Random generator;

        private MatrixGenerator()
        {
            this.generator = new Random();
        }

        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }
            return instance;
        }
        public double NextDouble()
        {
            return this.generator.NextDouble();
        }

        public static double[,] GenerateMatrix(int row, int column)
        {
            double[,] matrix = new double[row,column];
            for(int i = 0; i < row; i++)
            {
                for(int j = 0; j < column; j++)
                {
                    matrix[i, j] = GetInstance().NextDouble();
                }
            }
            return matrix;
            //ima dvije odgovornosti,u njoj i stvaramo i popunjavamo matricu
        }
    }
}
