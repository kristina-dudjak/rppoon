﻿using System;

namespace drugi
{
    class Program
    {
        static void Main(string[] args)
        {
            double[,] array = MatrixGenerator.GenerateMatrix(4, 5);
            for(int i = 0; i < 4; i++)
            {
                for(int j = 0; j < 5; j++)
                {
                    Console.WriteLine(array[i, j]);
                }
            }
        }
    }
}
