﻿using System;

namespace sedmi
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleNotification consoleNotification = new ConsoleNotification("Kristina", "sedmi", "tekst sedmog", DateTime.Now, Category.ALERT, ConsoleColor.DarkMagenta);
            ConsoleNotification notification = consoleNotification.Clone();
            Console.WriteLine(consoleNotification.ToString());
            Console.WriteLine(notification.ToString());
            

        }
    }
}
