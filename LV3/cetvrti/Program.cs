﻿using System;

namespace cetvrti
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationManager notificationManager = new NotificationManager();
            ConsoleNotification consoleNotification = new ConsoleNotification("Kristina", "Cetvrti", "Tekst", DateTime.Now, Category.ALERT, ConsoleColor.DarkMagenta);
            notificationManager.Display(consoleNotification);

            ConsoleNotification consoleNotification2 = new ConsoleNotification("Kristina", "Cetvrti", "Tekst", DateTime.Now, Category.ERROR, ConsoleColor.DarkGray);
            notificationManager.Display(consoleNotification2);

            ConsoleNotification consoleNotification3 = new ConsoleNotification("Kristina", "Cetvrti", "Tekst", DateTime.Now, Category.INFO, ConsoleColor.DarkYellow);
            notificationManager.Display(consoleNotification3);
        }
    }
}