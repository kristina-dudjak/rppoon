﻿using System;
using System.Collections.Generic;
using System.Text;

namespace treci
    
{
    class Logger
    {
        private string filePath;

        //datoteka nam je postavljena u konstruktoru,zbog singletona imamo jedan objekt pa bi uporaba na drugom mjestu i dalje pisala u istu datoteku
        public Logger()
        {
            this.filePath = "C:\\Users\\Antonija\\source\\repos\\RPPOON\\LV3 SOLUTION\\treci\\bin\\Debug\\datoteka.txt";
        }
        public void setFilePath(string filepath)
        {
            this.filePath = filepath;
        }

        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath))
            {
                writer.WriteLine(message);
            }
        }
    }
}
