﻿using System;
using System.Collections.Generic;
using System.Text;

namespace prvi
{
    class Dataset : Prototype
    {
        private List<List<string>> data;

        public Dataset()
        {
            this.data = new List<List<string>>();
        }

        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }

        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }

        //potrebno je duboko kopiranje jer promjenom jednog objekta unutar liste
        //bi se promijenio i kopirani,jer dijele listu,znaci moramo stvoriti nove liste


        public Dataset(Dataset dataset)
        {
            data = new List<List<string>>();

            foreach (List<string> datum in dataset.data)
            {
                List<string> informations = new List<string>();

                foreach (string info in datum)
                {
                    informations.Add(info);
                }
                data.Add(informations);
            }
        }

        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void PrintData()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach(List<string> datum in data)
            {
                foreach(string info in datum)
                {
                    stringBuilder.Append(info);
                }
                stringBuilder.Append("\n");
            }
            Console.WriteLine(stringBuilder.ToString());
        }
        
        public Prototype Clone()
        {
            return (new Dataset(this));
        }
    }
}
