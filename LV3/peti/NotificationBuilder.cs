﻿using System;
using System.Collections.Generic;
using System.Text;

namespace peti
{
    //konkretan graditelj
    public class NotificationBuilder:IBuilder
    {
        private String Author;
        private String Title;
        private DateTime Time;
        private Category Level;
        private ConsoleColor Color;
        private String Text;

        public NotificationBuilder(String author,String title,DateTime time,Category level, ConsoleColor color,String text)
        {
            this.Author = author;
            this.Title = title;
            this.Time = time;
            this.Level = level;
            Console.ForegroundColor = color;
            this.Text = text;
        }
        public NotificationBuilder() { }

        public ConsoleNotification Build()
        {
            return new ConsoleNotification(Author, Title, Time, Level, Color, Text);
        }
        public IBuilder SetAuthor(String author)
        {
            this.Author = author;
            return this;
        }
        public IBuilder SetTitle(String title)
        {
            this.Title = title;
            return this;
        }
        public IBuilder SetTime(DateTime time)
        {
            this.Time = time;
            return this;
        }
        public IBuilder SetLevel(Category level)
        {
            this.Level = level;
            return this;
        }
        public IBuilder SetColor(ConsoleColor color)
        {
            Console.ForegroundColor = color;
            return this;
        }
        public IBuilder SetText(String text)
        {
            this.Text = text;
            return this;
        }
    }
}
