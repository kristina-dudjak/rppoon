﻿using System;

namespace peti
{
    class Program
    {
        static void Main(string[] args)
        {
            //NotificationBuilder notificationBuilder = new NotificationBuilder("Kristina", "PETI", DateTime.Now, Category.ALERT, ConsoleColor.DarkMagenta, "proba petog");
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetAuthor("Kristina");
            notificationBuilder.SetTitle("Peti zadatak");
            notificationBuilder.SetTime(DateTime.Now);
            notificationBuilder.SetLevel(Category.ALERT);
            notificationBuilder.SetColor(ConsoleColor.DarkMagenta);
            notificationBuilder.SetText("proba petog zadatka");
            ConsoleNotification consoleNotification = notificationBuilder.Build();
            Console.WriteLine(consoleNotification);
        }
    }
}
