﻿using System;
using System.Collections.Generic;
using System.Text;

namespace šesti
{
    public enum Category { ERROR, ALERT, INFO }
    public class Director
    {
        private IBuilder builder;

        public Director(IBuilder builder)
        {
            this.builder = builder;
        }


        public void ConstructErrorNotification(String Author)
        {
            builder.SetAuthor(Author).SetTitle("šesti zadatak").SetTime(DateTime.Now).SetLevel(Category.ERROR).SetColor(ConsoleColor.DarkCyan).SetText("šesti sa errorom");
        
        }
        public void ConstructAlertNotification(String Author)
        {
            builder.SetAuthor(Author).SetTitle("šesti zadatak").SetTime(DateTime.Now).SetLevel(Category.ALERT).SetColor(ConsoleColor.DarkRed).SetText("šesti sa alertom");
        }
        public void ConstructInfoNotification(String Author)
        {
            builder.SetAuthor(Author).SetTitle("šesti zadatak").SetTime(DateTime.Now).SetLevel(Category.INFO).SetColor(ConsoleColor.DarkYellow).SetText("šesti sa info");
        }
    }
}
