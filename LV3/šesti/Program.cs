﻿using System;

namespace šesti
{
    class Program
    {
        static void Main(string[] args)
        {
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            Director director = new Director(notificationBuilder);

            director.ConstructErrorNotification("Kristina");
            ConsoleNotification error = notificationBuilder.Build();
            Console.WriteLine(error);

            director.ConstructAlertNotification("Kristina");
            ConsoleNotification alert = notificationBuilder.Build();
            Console.WriteLine(alert);

            director.ConstructInfoNotification("Kristina");
            ConsoleNotification info = notificationBuilder.Build();
            Console.WriteLine(info);

        }
    }
}

