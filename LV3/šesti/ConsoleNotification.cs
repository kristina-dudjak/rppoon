﻿using System;
using System.Collections.Generic;
using System.Text;

namespace šesti
{
    public class ConsoleNotification
        {
            private String Author { get; set; }
            private String Title { get; set; }
            private DateTime Time { get; set; }
            private Category Level { get; set; }
            private ConsoleColor Color { get; set; }
            private String Text { get; set; }

            public ConsoleNotification(String author, String title, DateTime time, Category level, ConsoleColor color, String text)
            {
                this.Author = author;
                this.Title = title;
                this.Time = time;
                this.Level = level;
                this.Color = color;
                this.Text = text;
            }
            public override string ToString()
            {
                return Author + ": " + Title + " " + Time + " " + Level + " " + Text;
            }
    }
    
}
