﻿using System;
using System.Collections.Generic;
using System.Text;

namespace treci
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
