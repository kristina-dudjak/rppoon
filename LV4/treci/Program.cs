﻿using System;
using System.Collections.Generic;

namespace treci
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> Items = new List<IRentable>();
            RentingConsolePrinter printer = new RentingConsolePrinter();

            Video firstvideo = new Video("The Godfather");
            Book firstbook = new Book("first_book");

            Items.Add(firstvideo);
            Items.Add(firstbook);
            
            printer.DisplayItems(Items);
            printer.PrintTotalPrice(Items);

        }
    }
}
