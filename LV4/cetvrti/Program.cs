﻿using System;
using System.Collections.Generic;

namespace cetvrti
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> Items = new List<IRentable>();
            RentingConsolePrinter printer = new RentingConsolePrinter();

            Video firstvideo = new Video("The Godfather 1");
            Book firstbook = new Book("Harry Potter 1");
            Video secondvideo = new Video("The Godfather 2");
            Book secondbook = new Book("Harry Potter 2");
            
            HotItem firsthotItem = new HotItem(secondvideo);
            HotItem secondhotItem = new HotItem(secondbook);

            Items.Add(firstvideo);
            Items.Add(firstbook);
            Items.Add(firsthotItem);
            Items.Add(secondhotItem);

            printer.DisplayItems(Items);
            printer.PrintTotalPrice(Items);

            //pojavio se trending(zbog klase hotitem, te se cijena za svaki hotItem povecala za 1.99)
        }
    }
}
