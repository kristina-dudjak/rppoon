﻿using System;
using System.Collections.Generic;
using System.Text;

namespace cetvrti
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
