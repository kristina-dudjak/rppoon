﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sedmi
{
    class RegistrationValidator:IRegistrationValidator
    {
        private EmailValidator emailValidator;
        private PasswordValidator passwordValidator;

        public RegistrationValidator()
        {
            emailValidator = new EmailValidator();
            passwordValidator = new PasswordValidator(8);
        }

        public bool IsUserEntryValid(UserEntry entry)
        {
            return emailValidator.IsValidAddress(entry.Email) && passwordValidator.IsValidPassword(entry.Password);
        }
    }
}
