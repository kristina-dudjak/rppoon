﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sedmi
{
    interface IEmailValidatorService
    {
        bool IsValidAddress(String candidate);
    }
}
