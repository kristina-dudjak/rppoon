﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sedmi
{
    interface IRegistrationValidator
    {
        bool IsUserEntryValid(UserEntry entry);
    }
}
