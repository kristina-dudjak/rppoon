﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sedmi
{
    class EmailValidator : IEmailValidatorService
    {

        public bool IsValidAddress(String candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return ContainsAt(candidate) && EndsCorrectly(candidate);
        }

        private bool ContainsAt(String candidate)
        {
            return candidate.Contains("@");
        }

        private bool EndsCorrectly(string candidate)
        {
            return candidate.EndsWith(".com") || candidate.EndsWith(".hr");
        }
    }
}

