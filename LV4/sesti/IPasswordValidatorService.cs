﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sesti
{
    interface IPasswordValidatorService
    {
        bool IsValidPassword(String candidate);
    }
}
