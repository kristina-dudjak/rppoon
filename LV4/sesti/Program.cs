﻿using System;

namespace sesti
{
    class Program
    {
        static void Main(string[] args)
        {
            PasswordValidator correctpassword = new PasswordValidator(8);
            PasswordValidator wrongpassword = new PasswordValidator(3);

            Console.WriteLine(correctpassword.IsValidPassword("098786787AAa"));
            Console.WriteLine(correctpassword.IsValidPassword("012"));

            EmailValidator correctemail = new EmailValidator();
            EmailValidator wrongemail = new EmailValidator();

            Console.WriteLine(correctemail.IsValidAddress("kristina.dudjak@gmail.com"));
            Console.WriteLine(wrongemail.IsValidAddress("kristina.dudjak@gmail"));
        }
    }
}
