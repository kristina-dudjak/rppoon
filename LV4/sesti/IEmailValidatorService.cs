﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sesti
{
    interface IEmailValidatorService
    {
        bool IsValidAddress(String candidate);
    }
}
