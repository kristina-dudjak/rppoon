﻿using System;
using System.Collections.Generic;

namespace peti
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> Items = new List<IRentable>();
            RentingConsolePrinter printer = new RentingConsolePrinter();

            Video firstvideo = new Video("The Godfather 1");
            Book firstbook = new Book("Harry Potter 1");
            Video secondvideo = new Video("The Godfather 2");
            Book secondbook = new Book("Harry Potter 2");
            
            HotItem firsthotItem = new HotItem(secondvideo);
            HotItem secondhotItem = new HotItem(secondbook);

            Items.Add(firstvideo);
            Items.Add(firstbook);
            Items.Add(firsthotItem);
            Items.Add(secondhotItem);

            List<IRentable> flashSale = new List<IRentable>();
            foreach(IRentable item in Items)
            {
                DiscountItem discountItem = new DiscountItem(item, 70);
                flashSale.Add(discountItem);
            }

            printer.DisplayItems(flashSale);
            printer.PrintTotalPrice(flashSale);


        }
    }
}
