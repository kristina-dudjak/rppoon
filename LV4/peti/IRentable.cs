﻿using System;
using System.Collections.Generic;
using System.Text;

namespace peti
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
