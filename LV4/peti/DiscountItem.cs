﻿using System;
using System.Collections.Generic;
using System.Text;

namespace peti
{
    class DiscountItem:RentableDecorator
    {
        private readonly double DiscountPercentage;

        public DiscountItem(IRentable rentable,double discountItem) : base(rentable)
        {
            this.DiscountPercentage = discountItem;
        }

        public override double CalculatePrice()
        {
            return base.CalculatePrice() - base.CalculatePrice() * DiscountPercentage * 0.01;
        }

        public override String Description
        {
            get
            {
                return base.Description + " now at " + DiscountPercentage + "% off!";
            }
        }
    }
}
