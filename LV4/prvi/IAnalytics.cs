﻿using System;
using System.Collections.Generic;
using System.Text;

namespace prvi_drugi
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);
    }
}