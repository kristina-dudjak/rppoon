﻿using System;
using System.Collections.Generic;
using System.Text;

namespace prvi_drugi
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;

        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }

        private double[][] ConvertData(Dataset dataset)
        {
            double[][] array = new double[dataset.GetData().Count][];
            for(int i = 0; i < dataset.GetData().Count; i++)
            {
                array[i] = dataset.GetData()[i].ToArray();
            }
            return array;
        }

        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }

        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
