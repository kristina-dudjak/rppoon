﻿using System;

namespace prvi_drugi
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;

            Dataset dataset = new Dataset("C:\\Users\\Antonija\\source\\repos\\RPPOON\\LV4\\prvi\\CSVFile.txt");

            Analyzer3rdParty service = new Analyzer3rdParty();

            Adapter adapter = new Adapter(service);

            Print(adapter.CalculateAveragePerColumn(dataset));
            Print(adapter.CalculateAveragePerRow(dataset));

            double[][] array = new double[][]{
                new double[] {1.5, 2.5, 3.5},
                new double[] {12.6, 56.7, 78.9},
                new double[] {5.8, 99.9, 3.567}
             };

            Print(service.PerRowAverage(array));
            Print(service.PerColumnAverage(array));
        }

        static void Print(double[] array)
        {
            for(int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] );
                Console.Write(" ");
            }
            Console.WriteLine();

        }
    }
}
