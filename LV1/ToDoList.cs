﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp5
{
    class ToDoList
    {
        private List<TimeNote> notes=new List<TimeNote>();
        
        public void AddNote(TimeNote note)
        {
            notes.Add(note);
        }
        public void RemoveNote(TimeNote note)
        {
            notes.Remove(note);
        }
        public Note GetNote(int index)
        {
            return notes[index];
        }
        public override string ToString()
        {
            string list="";
            foreach (TimeNote note in notes)
                list += note;
            return list;
        }
        public void doHighestTasks()
        {
            foreach(TimeNote note in notes.ToList())

            {
                if (note.isHighImportance())
                {
                    RemoveNote(note);
                }
            }
        }
    }
}
