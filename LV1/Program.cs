﻿using System;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Note first_note,second_note,third_note;
            TimeNote first_timeNote, second_timeNote, third_timeNote;
            first_note = new Note();
            Console.WriteLine(first_note.getAuthor());
            Console.WriteLine(first_note.getTxt());
            second_note = new Note("John's text", "John", "high");
            Console.WriteLine(second_note.getAuthor());
            Console.WriteLine(second_note.getTxt());
            third_note = new Note(first_note);
            Console.WriteLine(third_note.getAuthor());
            Console.WriteLine(third_note.getTxt());

            ToDoList toDoList;
            toDoList = new ToDoList();
            first_timeNote = new TimeNote();
            second_timeNote = new TimeNote(new DateTime(2020, 3, 1, 11, 0, 0), "Mark's text", "Mark", "low");
            third_timeNote = new TimeNote(new DateTime(2020, 6, 7, 18, 30, 0), "Julia's text", "Julia", "high");

            toDoList.AddNote(first_timeNote);
            toDoList.AddNote(second_timeNote);
            Console.WriteLine(toDoList.ToString());
            toDoList.doHighestTasks();
            Console.WriteLine(toDoList.ToString());
        }
        
    }
}
