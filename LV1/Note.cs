﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp5
{
    class Note
    {
        private string txt;
        private string author;
        private string level_of_importance;

        public string getTxt() { return this.txt; }
        public string getAuthor() { return this.author; }
        public string getLevel_of_importance() { return this.level_of_importance; }
        public void setTxt(string txt) { this.txt = txt; }
        public void setLevel_of_importance(string level_of_importance) { this.level_of_importance = level_of_importance; }
        public Note()
        {
            this.txt = "Kristina's text";
            this.author = "Kristina";
            this.level_of_importance = "high";
        }
        public Note(string txt, string author, string level_of_importance)
        {
            this.txt = txt;
            this.author = author;
            this.level_of_importance = level_of_importance;
        }
        public Note(Note CopyNote) {
            txt = CopyNote.txt;
            author = CopyNote.author;
            level_of_importance = CopyNote.level_of_importance;
        }
        public string Txt
        {
            get { return this.txt; }
            private set { this.txt = value; }
        }
        public string Author
        {
            get { return this.author; }
        }
        public string Level_of_importance
        {
            get { return this.level_of_importance; }
            private set { this.level_of_importance = value; }
        }
        public override string ToString()
        {
            return this.Author +" "+ this.Txt+"\n";
        }
        public bool isHighImportance()
        {
            return level_of_importance == "high";
        }


    }
}
