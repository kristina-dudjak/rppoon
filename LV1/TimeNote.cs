﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp5
{
    class TimeNote: Note
    {
        private DateTime dateTime;
        
        public TimeNote(DateTime date, string txt, string author, string level_of_importance) : base(txt, author, level_of_importance)
        {
            this.dateTime = date;
        }
        public TimeNote()
        {
            this.dateTime = DateTime.Now;
        }
        public void setDateTime(DateTime date) { this.dateTime = date; }
        public override string ToString()
        {
            return this.Author+" " + this.Txt+" " + this.dateTime+"\n";
        }
       


    }
}
