﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2
{ 
    class Die
    {
        private int numberOfSides;
        private RandomGenerator randomGenerator;
        /*private Random randomGenerator;

        public Die(int numberOfSides,Random generator)    //za 2.zadatak u konstruktoru dodan generator
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = generator;
        } 
        */ 
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }

        public int Roll()
        {

            int rolledNumber;
            //rolledNumber = randomGenerator.Next(1, numberOfSides + 1);    //za prva dva zadatka
            rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);   //za treci zadatak
            
            return rolledNumber;

        }
    }
}
