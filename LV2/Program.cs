﻿using System;
using System.Text;

namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            Random rand = new Random();
            for (int i = 0; i < 20; i++)
            {
                // Die die = new Die(6,rand);     //za 2. zadatak zadajemo i drugi parametar,generator
                Die die = new Die(6);             //treći zadatak
                diceRoller.InsertDie(die);
            }
            diceRoller.RollAllDice();
            //PrintDiceRoller(diceRoller);
            //diceRoller.LogRollingResults();
            Console.WriteLine(diceRoller.GetStringRepresentation());
        }

         static void PrintDiceRoller(DiceRoller diceRoller)
        {
            for (int i = 0; i < diceRoller.DiceCount; i++)
            {
                Console.WriteLine(diceRoller.GetRollingResults()[i]);
            }
        }
        


    }
}
