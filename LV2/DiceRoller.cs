﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV2
{
    class DiceRoller:ILogable
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        //private ILogger logger;

        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
            //this.logger = new FileLogger("C:\\Users\\Antonija\\Desktop\\faks\\RPPOON\\file.txt");
            //this.logger = new ConsoleLogger();
        }
        /*
        public void setLogger(ILogger logger)
        {
            this.logger = logger;
        }
        */
        
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDice()
        {
            //clear results of previous rolling
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        //View of the results
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
                this.resultForEachRoll
           );
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }
        
        /*
        public void LogRollingResults()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (int result in this.resultForEachRoll)
            {
                stringBuilder.Append(result).Append("\n");
            }
            logger.Log(stringBuilder.ToString());
        }
        */

        public string GetStringRepresentation()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach(int result in this.resultForEachRoll)
            {
                stringBuilder.Append(result).Append("\n");
            }
            return stringBuilder.ToString();
        }
        

    }
}
